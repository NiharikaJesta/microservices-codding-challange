package com.gl.assignment.CourseCatalogService.model;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class RemoteEnrolledCourseConsumer {

	@Autowired
	private RestTemplate restTemplate;
	private String baseURL;
	
	public RemoteEnrolledCourseConsumer() {
		// TODO Auto-generated constructor stub
	}

	public RemoteEnrolledCourseConsumer(String baseURL) {
		super();
		this.baseURL = baseURL.startsWith("http://") ? baseURL : "http://"+baseURL;
	}
	
	public List<Course> getAllUserCourse(String id){
		Course[] courses = restTemplate.getForObject(baseURL+"/course/{id}", Course[].class, id);
		return Arrays.asList(courses);
	}
}
