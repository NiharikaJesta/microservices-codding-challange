package com.gl.assignment.CourseCatalogService.model;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

public class RemoteCourseRatingConsumer {

	@Autowired
	private RestTemplate restTemplate;
	private String baseURL;
	
	public RemoteCourseRatingConsumer() {
		// TODO Auto-generated constructor stub
	}

	public RemoteCourseRatingConsumer(String baseURL) {
		super();
		this.baseURL = baseURL.startsWith("http://") ? baseURL : "http://"+baseURL;
	}
	
	public List<CourseFeedback> getAllRatingsAndComments(String courseid){
		CourseFeedback[] feedbacks = restTemplate.getForObject(baseURL+"/allratings/{id}", CourseFeedback[].class, courseid);
		return Arrays.asList(feedbacks);
	}
}
