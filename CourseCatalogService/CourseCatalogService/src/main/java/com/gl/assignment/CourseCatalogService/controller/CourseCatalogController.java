package com.gl.assignment.CourseCatalogService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gl.assignment.CourseCatalogService.model.Course;
import com.gl.assignment.CourseCatalogService.model.CourseFeedback;
import com.gl.assignment.CourseCatalogService.model.RemoteCourseRatingConsumer;
import com.gl.assignment.CourseCatalogService.model.RemoteEnrolledCourseConsumer;

@RestController
public class CourseCatalogController {

	@Autowired
	private RemoteEnrolledCourseConsumer consumer;
	
	@Autowired
	private RemoteCourseRatingConsumer ratingConsumer;
	
	@GetMapping("/enrolledCourses/{userid}")
	public List<Course> getAllUserCourse(@PathVariable("userid")String id){
		return consumer.getAllUserCourse(id);
	}
	
	@GetMapping("/ratings/{courseid}")
	public List<CourseFeedback> getAllRatings(@PathVariable("courseid")String id){
		return ratingConsumer.getAllRatingsAndComments(id);
	}
}
