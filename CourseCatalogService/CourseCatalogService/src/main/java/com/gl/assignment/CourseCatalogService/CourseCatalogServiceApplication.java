package com.gl.assignment.CourseCatalogService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.gl.assignment.CourseCatalogService.model.RemoteCourseRatingConsumer;
import com.gl.assignment.CourseCatalogService.model.RemoteEnrolledCourseConsumer;

@SpringBootApplication
@EnableEurekaClient
public class CourseCatalogServiceApplication {

	private static final String 
	ENROLLED_COURSE_URL = "http://ENROLLED-COURSE-MICROSERVICES-PRODUCER";
	
	private static final String 
	COURSE_RATING_URL = "http://COURSE-RATING-MICROSERVICES-PRODUCER";
	
	public static void main(String[] args) {
		SpringApplication.run(CourseCatalogServiceApplication.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public RemoteEnrolledCourseConsumer getEnrolledCourseConsumer() {
		return new RemoteEnrolledCourseConsumer(ENROLLED_COURSE_URL);
	}
	
	@Bean
	public RemoteCourseRatingConsumer getCourseRatingConsumer() {
		return new RemoteCourseRatingConsumer(COURSE_RATING_URL);
	}
	
}
