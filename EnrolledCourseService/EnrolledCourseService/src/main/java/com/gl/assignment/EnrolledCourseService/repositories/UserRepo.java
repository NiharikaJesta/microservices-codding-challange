package com.gl.assignment.EnrolledCourseService.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gl.assignment.EnrolledCourseService.model.*;

public interface UserRepo extends JpaRepository<User, Integer>{

}
