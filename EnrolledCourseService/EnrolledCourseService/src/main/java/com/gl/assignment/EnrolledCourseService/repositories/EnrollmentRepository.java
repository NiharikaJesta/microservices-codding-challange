package com.gl.assignment.EnrolledCourseService.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.gl.assignment.EnrolledCourseService.model.Course;
import com.gl.assignment.EnrolledCourseService.model.Enrollment;
import com.gl.assignment.EnrolledCourseService.model.User;


public interface EnrollmentRepository extends JpaRepository<Enrollment, Integer>{
	
	@Query("select e.course from Enrollment e where e.user = ?1")
	public List<Course> getAllCourse(User user);
}
