package com.gl.assignment.EnrolledCourseService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gl.assignment.EnrolledCourseService.model.Course;
import com.gl.assignment.EnrolledCourseService.model.User;
import com.gl.assignment.EnrolledCourseService.repositories.EnrollmentRepository;

@RestController
public class EnrollmentController {

	@Autowired
	private EnrollmentRepository enrollmentRepository;

	
	@RequestMapping("/course/{id}")
	public List<Course> getUserCourse(@PathVariable("id")int id){
		return enrollmentRepository.getAllCourse(new User(id));
	}
}
