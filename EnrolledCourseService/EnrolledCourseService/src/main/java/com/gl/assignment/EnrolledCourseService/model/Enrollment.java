package com.gl.assignment.EnrolledCourseService.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Enrollment {
	
	@Id
	private int enrollmentid;
	@ManyToOne
	private User user;
	@ManyToOne
	@JoinColumn(name = "courseId")
	private Course course;
	public int getEnrollmentid() {
		return enrollmentid;
	}
	public void setEnrollmentid(int enrollmentid) {
		this.enrollmentid = enrollmentid;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	
}
