package com.gl.assignment.EnrolledCourseService.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "CourseUser")
public class User {

	@Id
	private int userid;
	private String username;
	private String password;

	public User() {
		// TODO Auto-generated constructor stub
	}
	
	public User(int userid) {
		super();
		this.userid = userid;
	}


	public int getCourseid() {
		return userid;
	}

	public void setCourseid(int userid) {
		this.userid = userid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
