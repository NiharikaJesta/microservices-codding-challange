package com.gl.assignment.EnrolledCourseService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EnrolledCourseServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(EnrolledCourseServiceApplication.class, args);
	}

}
