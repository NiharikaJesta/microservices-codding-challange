package com.gl.assignment.CourseRatingService.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.gl.assignment.CourseRatingService.model.Course;
import com.gl.assignment.CourseRatingService.model.CourseFeedback;

public interface CourseFeedbackRepo extends JpaRepository<CourseFeedback, Integer>{
	
	@Query("select feedback from CourseFeedback feedback where feedback.course = ?1")
	public List<CourseFeedback> getAllRatingsAndComments(Course course);
}
