package com.gl.assignment.CourseRatingService.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.gl.assignment.CourseRatingService.model.Course;
import com.gl.assignment.CourseRatingService.model.CourseFeedback;
import com.gl.assignment.CourseRatingService.repositories.CourseFeedbackRepo;

@RestController
public class CourseRatingController {

	@Autowired
	private CourseFeedbackRepo courseFeedbackRepo;
	
	@GetMapping("/allratings/{courseid}")
	public List<CourseFeedback> getAllRatings(@PathVariable("courseid")int id)
	{
		return courseFeedbackRepo.getAllRatingsAndComments(new Course(id));
	}
}
