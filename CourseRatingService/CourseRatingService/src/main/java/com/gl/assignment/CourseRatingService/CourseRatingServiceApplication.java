package com.gl.assignment.CourseRatingService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CourseRatingServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CourseRatingServiceApplication.class, args);
	}

}
