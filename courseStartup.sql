use microservice;

insert into course_user values(101, 'pavan', 'pavan');
insert into course_user values(102, 'sai', 'sai');
 

insert into courses values(301, 'java', '2hrs', '10000');
insert into courses values(302, 'python', '2.5hrs', '15000');


insert into enrollment values(501, 301, 101);
insert into enrollment values(502, 302, 101);


insert into course_rating values(901, 'great course', 5, 301, 101);
insert into course_rating values(902, 'great material', 4, 302, 101);
insert into course_rating values(903, 'great material', 4, 301, 102);



